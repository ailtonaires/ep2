

# EP2 - OO 2019.2 (UnB - Gama)

Turmas Renato e Carla
Data de entrega: 05/11/2019

Nome: Ailton Aires
Matrícula: 18/0011600

## Descrição

O projeto visa a implementação do jogo Snake em POO. O objetivo do jogo é basicamente comer o máximo de frutas possíveis. Conforme as frutas vão sendo 
comidas pela Snake, o nível de dificuldade vai aumentando. No jogo só foi implementado a Snake comum e a SimpleFruit.

##Versão Java

- Java SE JDK 11.0.4

##Jogar

- Inicie a pasta do projeto com o Eclipse 

##Versão Eclipse

- Version: 2019-03 (4.11.0)
Build id: 20190314-1200

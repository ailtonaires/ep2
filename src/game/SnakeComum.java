package game;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public class SnakeComum extends Snake {
	
	private int tamanho = 10;
	public int local_x, local_y, size;
	
	

	public SnakeComum(int size) {
		
		this.size = size;
		
	}



	public int getLocal_x() {
		return local_x;
	}



	public void setLocal_x(int local_x) {
		this.local_x = local_x;
	}



	public int getLocal_y() {
		return local_y;
	}



	public void setLocal_y(int local_y) {
		this.local_y = local_y;
	}



	public int getTamanho() {
		return tamanho;
	}



	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}
	
	public Rectangle getBound() {
		
		return new Rectangle(local_x, local_y, size, size );
	}
	
	public boolean isCollision(SnakeComum o) {
		
		if(o == this)return false;
		return getBound().intersects(o.getBound());
	}

	public void render (Graphics2D g2d) {
		
		g2d.fillRect(local_x+1, local_y+1, size-2, size-2);
	}



	public void setPosition(int local_x, int local_y) {
		
		this.local_x = local_x;
		this.local_y = local_y;
		
		
	}



	public void movimenta(int x, int y) {

		local_x += x ;
		local_y += y;
	}

}

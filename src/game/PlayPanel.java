package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


import java.awt.Canvas;



@SuppressWarnings("serial")
public class PlayPanel extends JPanel implements Runnable, KeyListener { 
	
	private Thread loop;
	private boolean rodar_jogo, finish;
	private boolean cima, baixo, esquerda, direita;
	private long tempo;
	private static int largura = 650;
	private static int altura = 650;
	private static final int tam = 10;
	private int pontuacao = 0;
	private int coord_y, coord_x, time = 800;
	private Graphics2D graficos;
	private BufferedImage imagem; 
	SnakeComum cabeca;
	SnakeComum colisao, bomb;
	ArrayList <SnakeComum> cobra;
	
	
	public PlayPanel() {
		setPreferredSize(new Dimension(largura, altura));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
		
		this.addKeyListener(this);
		
	}
	
	
	
	public void Tela_Principal() {
		
		JFrame janela = new JFrame ("SNAKE GAME");
		janela.setContentPane(new PlayPanel());
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setResizable(false);
		janela.pack();

		janela.setPreferredSize(new Dimension(PlayPanel.altura, PlayPanel.largura));
		janela.setLocationRelativeTo(null);
		janela.setVisible(true);
		
		

	}
	
	
	public void addNotify() {
		super.addNotify();
		loop = new Thread(this);
		loop.start();
		
	}
	
	

	@Override
	public void keyTyped(KeyEvent e) { 
		
		


	}

	@Override
	public void keyPressed(KeyEvent input) {
		
		int key = input.getKeyCode();
		
		if (key == KeyEvent.VK_UP) cima = true;
		if (key == KeyEvent.VK_DOWN) baixo = true;
		if (key == KeyEvent.VK_LEFT) esquerda = true;
		if (key == KeyEvent.VK_RIGHT) direita = true;
		//if (key == KeyEvent.VK_ENTER) start = true;


	}

	@Override
	public void keyReleased(KeyEvent input) {
		
		int key = input.getKeyCode();
		
		if (key == KeyEvent.VK_UP) cima = false;
		if (key == KeyEvent.VK_DOWN) baixo = false;
		if (key == KeyEvent.VK_LEFT) esquerda = false;
		if (key == KeyEvent.VK_RIGHT) direita = false;
	//	if (key == KeyEvent.VK_ENTER) start = true;


	}
	
	public void cresce_cobra() {
		
		cobra = new ArrayList<SnakeComum>();
		cabeca = new SnakeComum(tam);
		cabeca.setPosition(largura/2, altura/2);
		cobra.add(cabeca);
		
		for(int i=0; i<2; i++) {	
			
			SnakeComum new_snake = new SnakeComum(tam);
			new_snake.setPosition(cabeca.getLocal_x() + i*tam, cabeca.getLocal_y() );
			cobra.add(new_snake);
		}
		
		colisao = new SnakeComum(tam);
		setApple();
		finish = false;
		
	}
	


	@Override
	public void run() {
			 
		if (rodar_jogo)return;
			init();	
		
			while(true) {
				
			setatualiza();
			waitrenderiza();
			try {
				Thread.sleep(time/6);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			}

	}
	
	
	private void waitrenderiza() {
	
		
		renderiza(graficos);
		Graphics grafico = getGraphics();
		grafico.drawImage(imagem, 0, 0, null);
	
		
	}



	private void setatualiza() {
		
	//	if (finish) {
		//	pontuacao = 10;
		//}
		
		
		if (cima && coord_y == 0) {
			coord_y = -tam;
			coord_x =0;
		}
		
		if (baixo && coord_y == 0) {
			
			coord_y = tam;
			coord_x = 0;
		}
		
		if (esquerda && coord_x == 0) {
			coord_y = 0;
			coord_x = -tam;
		}
		
		if (direita && coord_x == 0 && coord_y != 0) {
			
			coord_y = 0;
			coord_x = tam;
		}
		
		if (coord_x !=0 || coord_y != 0) {
			
			for (int i = cobra.size()-1; i > 0; i--) {
			
				cobra.get(i).setPosition(cobra.get(i-1).getLocal_x(), cobra.get(i-1).getLocal_y());
			}
			
			cabeca.movimenta(coord_x, coord_y);
		}
		
		for (SnakeComum e: cobra) {
			if (e.isCollision(cabeca)) {
				finish = true;
				break;
			}
		}
		
		if (colisao.isCollision(cabeca)){
			
			setApple();
			
			SnakeComum add = new SnakeComum(tam);
			add.setPosition(-100, -100);
			cobra.add(add);
			pontuacao++;
			time -=3;
			
		}
		
		
		if (cabeca.getLocal_x()<0)
			cabeca.setLocal_x(largura);
		
		if (cabeca.getLocal_y()<0)
			cabeca.setLocal_y(altura);
		
		if (cabeca.getLocal_x() > largura)
			cabeca.setLocal_x(0);
		
		if (cabeca.getLocal_y()>altura)
			cabeca.setLocal_y(0);
		
		
		}
	
	public void setApple() {
		
		int x = (int)(Math.random()*(largura-tam));
		int y = (int)(Math.random()*(largura-tam));
		colisao.setPosition(x,y); 


	}

	private void renderiza(Graphics2D grafico) {
		
		grafico.clearRect(0, 0, altura, largura);
		grafico.setColor(Color.WHITE);
		
		for(SnakeComum snakes : cobra) {
			snakes.render(grafico);
		}
		
		grafico.setColor(Color.RED);
		colisao.render(grafico);
		
		grafico.setColor(Color.RED);
		colisao.render(grafico);
		
		grafico.setColor(Color.GREEN);
		grafico.drawString("Pontuação: " + pontuacao, 10,640);

		
		
		
		
		
	}	



	public void init() {
		
		imagem = new BufferedImage(altura, largura, BufferedImage.TYPE_INT_ARGB);
		graficos = imagem.createGraphics();
		rodar_jogo = true;
		cresce_cobra();
		waitrenderiza();
		
		
	}



	public boolean isRodar_jogo() {
		return rodar_jogo;
	}



	public void setRodar_jogo(boolean rodar_jogo) {
		this.rodar_jogo = rodar_jogo;
	}



	public long getTempo() {
		return tempo;
	}

	
	public int getLargura() {
		return largura;
	}
	
	public int getAltura() {
		return altura;
	}
	

}

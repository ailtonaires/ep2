package game;

import java.util.Random;

public class SimpleFruit {
	
	private int pos_x , pos_y, size = 10 ;
	private PlayPanel obj;

	public SimpleFruit() {
		
	}

	public int getPos_x() {
		return pos_x = (int)(Math.random()*(obj.getLargura()-size));
	}

	public void setPos_x() {

		pos_x = (int)(Math.random()*(obj.getLargura()-size));
	}

	public int getPos_y() {
		return pos_y = (int)(Math.random()*(obj.getAltura()-size));
	}

	public void setPos_y() {
		
		pos_y = (int)(Math.random()*(obj.getAltura()-size));
	}
	
	public void posicao(int x, int y) {
		
		this.pos_x = x;
		this.pos_y = y;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}

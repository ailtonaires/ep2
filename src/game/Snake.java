package game;

public class Snake {
	
	private int tamanho;
	private int direcao_x;
	private int direcao_y;
	
	

	public Snake() {
		

	}


	public int getTamanho() {
		return tamanho;
	}
	
	
	public int getDirecao_x() {
		return direcao_x;
	}
	
	
	public int getDirecao_y() {
		return direcao_y;
	}


	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}


	public void setDirecao_x(int direcao_x) {
		this.direcao_x = direcao_x;
	}


	public void setDirecao_y(int direcao_y) {
		this.direcao_y = direcao_y;
	}
	
	

}
